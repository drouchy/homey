Rails.application.routes.draw do
  resources :projects, only: %i[index show update] do
    resources :comments, only: %i[create]
  end
  
  root "projects#index"
end
