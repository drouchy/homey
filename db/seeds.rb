
backend = FactoryBot.create(:project, name: 'Homey backend')
FactoryBot.create(:comment, project: backend, text: "this is great")
FactoryBot.create(:comment, project: backend, text: "this is awesome")
FactoryBot.create(:comment, project: backend, text: "this is not bad")
FactoryBot.create(:comment, project: backend, text: "a very good project\nwe should carry on")


FactoryBot.create(:project, name: 'Homey frontend')

spike = FactoryBot.create(:project, :archived, name: 'Homey spike')
FactoryBot.create(:comment, project: spike, text: "spike completed")