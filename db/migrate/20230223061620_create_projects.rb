class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false, index: {unique: true}
      t.string :status, null: false, default: 'active'

      t.timestamps
    end
  end
end
