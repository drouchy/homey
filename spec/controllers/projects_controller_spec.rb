require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do

  describe ".index" do
    let!(:projects) do
      [
        create(:project, name: 'The first project'),
        create(:project, name: 'A project'),
        create(:project, :archived, name: 'Very last minute project')
      ]  
    end
    
    it "assigns @projects containing all the projects ordered by name" do
      get :index
      
      expect(assigns(:projects).map(&:name)).to eq ['A project', 'The first project', 'Very last minute project']
    end
    
    it "renders the index template" do
      get :index
      
      expect(response.status).to eq(200)
      expect(response).to render_template 'index'
      expect(response.content_type).to eq 'text/html; charset=utf-8'
    end
  end
end