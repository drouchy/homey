require 'rails_helper'

RSpec.describe CommentsController, type: :controller do

  describe ".index" do
    let(:project) { create(:project) }
    
    it "creates a comment on linked to the project" do
      post :create, params: { comment: { text: "the comment"}, commit: "Comment", project_id: project.id }
      
      expect(response.content_type).to eq 'text/html; charset=utf-8'
      
      expect(Comment.count).to eq(1)
    end
    
    it "rejects creating a comment for an archived project" do
      project.update(status: 'archived')
      
      post :create, params: { comment: { text: "the comment"}, commit: "Comment", project_id: project.id }
      
      expect(response.content_type).to eq 'text/html; charset=utf-8'
      
      expect(Comment.count).to eq(0)
    end
  end
end