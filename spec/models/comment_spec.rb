require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'validation' do
    it 'validates the content' do
      expect(build(:comment, text: 'x')).to be_valid
      expect(build(:comment, text: 'this is a comment')).to be_valid
      expect(build(:comment, text: 'x' * 500)).to be_valid
      
      expect(build(:comment, text: nil)).to_not be_valid
      expect(build(:comment, text: '')).to_not be_valid
      expect(build(:comment, text: 'x' * 501)).to_not be_valid
    end
    
    it 'validates a comment needs to belong to a project' do
      expect(build(:comment, project: build(:project))).to be_valid
      
      expect(build(:comment, project: nil)).to_not be_valid
    end
  end
end
