require 'rails_helper'

RSpec.describe Project, type: :model do
  describe "validation" do
    it "validates the name of the project" do
      expect(build(:project, name: 'Homey')).to be_valid
      
      expect(build(:project, name: '')).to_not be_valid
      expect(build(:project, name: nil)).to_not be_valid
    end
    
    it "validates the uniqueness of the project name" do
      create(:project, name: 'Homey')
      
      expect { create(:project, name: 'Homey') }.to raise_error(ActiveRecord::RecordNotUnique)
    end
    
    it "validates the status of the project" do
      expect(build(:project, status: 'active')).to be_valid
      expect(build(:project, status: 'archived')).to be_valid
      expect(build(:project, status: :active)).to be_valid
      expect(build(:project, status: :archived)).to be_valid
      
      expect(build(:project, status: '')).to_not be_valid
      expect(build(:project, status: nil)).to_not be_valid
      expect(build(:project, status: 'something else')).to_not be_valid
      expect(build(:project, status: 'ARCHIVED')).to_not be_valid
    end
  end
  
  describe ".status" do
    it "allows to archive an active project" do
      project = create(:project, status: :active)
      
      project.update(status: 'archived')
      
      expect(project.status).to eq 'archived'
    end
    
    it "allows to activete an archived project" do
      project = create(:project, :archived)
      
      project.update(status: 'active')
      
      expect(project.status).to eq 'active'
    end
  end
  
  describe ".flip_status" do
    it "it archives an active project" do
      project = create(:project, status: :active)
      
      project.flip_status
      
      expect(project.status).to eq 'archived'
    end
    
    it "it actives an archived project" do
      project = create(:project, :archived)
      
      project.flip_status
      
      expect(project.status).to eq 'active'
    end
  end
  
  describe ".comments" do
    let(:project) { create(:project) }
    
    it "orders the comments from the newest to the oldest based on the creation date" do
      create(:comment, project:, text: "comment_1", created_at: 2.days.ago)
      create(:comment, project:, text: "comment_2", created_at: Date.current)
      create(:comment, project:, text: "comment_3", created_at: 6.days.ago)
      
      expect(project.comments.map(&:text)).to eq %w[comment_2 comment_1 comment_3]
    end
  end
end
