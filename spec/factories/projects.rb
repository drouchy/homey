FactoryBot.define do
  factory :project do
    name { "Homey" }
    status { "active" }

    trait :archived do
      status { "archived" }
    end
  end
end