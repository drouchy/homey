FactoryBot.define do
  factory :comment do
    text { "MyText" }
    
    project { build(:project) }
  end
end
