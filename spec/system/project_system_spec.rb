require 'rails_helper'

RSpec.describe "Comment a project", type: :system do
  before do
    driven_by(:selenium_chrome_headless)
  end

  describe "project list" do
    let!(:projects) do
      [
        create(:project, name: 'The first project'),
        create(:project, name: 'A project'),
        create(:project, :archived, name: 'Very last minute project')
      ]
    end
    
    it "renders the project name and link" do
      visit root_path
      
      expect(page).to have_text 'The first project'
      expect(page).to have_text 'Very last minute project (archived)'
      
      projects.each do |project|
        expect(page.html).to include project_path(project)
      end
    end
  end
  
  describe "project show" do
    let(:project) { create(:project, name: 'The first project') }
    
    it "renders the project name" do
      visit project_path(project)
      
      expect(page).to have_text 'The first project'
    end
    
    it "allows to archive / activate the project" do
      visit project_path(project)
      
      click_button "Archive"
      
      expect(page).to have_text 'The first project (archived)'
      expect(project.reload.status).to eq 'archived'
      
      click_button "Activate"
      
      expect(page).to have_text 'The first project'
      expect(page).to_not have_text 'archived'
      expect(project.reload.status).to eq 'active'
    end
  end
  
  describe "project comments" do
    let(:project) { create(:project, name: 'The first project', comments: [build(:comment, text: "this is great")]) }
    
    it "renders the project comments" do
      visit project_path(project)
      
      expect(page).to have_text 'this is great'
    end
  end
  
  describe "Add a comment" do
    let(:project) { create(:project) }
    let(:comment_text) { 'This is a comment for the project' }
    
    it "allows a user to comment a project" do
      visit project_path(project)
      
      fill_in 'comment_text', with: comment_text
      click_button 'Comment'
      
      expect(page).to have_text comment_text
      expect(Comment.count).to eq 1
    end
    
    it "does not allow to comment an archived project" do
      project.update(status: 'archived')
      
      visit project_path(project)
      
      button = find_button('Comment', disabled: true)
      
      expect(button).to be_disabled
    end
  end
end
