class Comment < ApplicationRecord
  validates :text, presence: true, length: {in: 1..500}
  
  belongs_to :project
end
