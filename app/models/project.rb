# frozen_string_literal: true

class Project < ApplicationRecord
  validates :name, presence: true
  validates :status, presence: true, inclusion: { in: %w[active archived] }
  
  has_many :comments, -> { order(created_at: :desc) }

  def flip_status
    update(status: status == 'active' ? 'archived' : 'active')
  end
end
