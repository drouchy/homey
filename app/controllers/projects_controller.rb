# frozen_string_literal: true

class ProjectsController < ApplicationController
  def index
    @projects = Project.all.order(name: :asc)
  end
  
  def show
    @project = Project.find(params[:id])
  end
  
  def update
    @project = Project.find(params[:id])
    @project.flip_status
    
    redirect_to project_path(@project)
  end
end