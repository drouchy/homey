class CommentsController < ApplicationController
  def create
    project = Project.find(params[:project_id])
    
    Comment.create(comment_params.merge(project:)) if project.status == 'active'


    redirect_to project_url(project)
  end
  
  private

  def comment_params
    params.fetch(:comment, {}).permit(:text, :project_id)
  end
end
