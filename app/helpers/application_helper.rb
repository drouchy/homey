module ApplicationHelper

  def project_name(project)
    case project.status
    when 'archived'
      "#{project.name} (archived)"
    else
      project.name
    end
  end
end
