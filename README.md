# Task

Use Ruby on Rails to build a project conversation history. A user should be able to:

- leave a comment
- change the status of the project

The project conversation history should list comments and changes in status.

# Brief

* How do we create projects?
  `later we should be able to do that via the app, but for now the console of db seed is sufficient`
  
* What are the possible statuses for a project?
  `active and archived`

* Can we move freely from active to archived? ie. can we un-archived a project?
  `yes, if a project has been archived by mistake, we can re-active it`
  
* Can we still comment a archived project?
  `no`
  
* Can everybody change a project? comment a project?
  `for now yes. it's an internal tool`
  
* is there a limit in the size of comments?
  `we can set a limit to around 500 characters for now`
  
* What kind of text formatting do we want for the comments? HTML, markdown...
  `plain text for the first version`
  
* What's the preferred way to present the comments>? Oldest first, or newest first?
  `from the latest to the oldest`
  

# Up and running

_the project requires ruby and bundler to be installed on your system_

1. bundle install
2. ./bin/rails db:migrate
3. ./bin/rails db:seed
4. ./bin/rails server

the app should be accessible at http://localhost:3000
  
# Future development

the project has been developed in 2 hours. There are few features missing

* support error and notice via a banner
* navigation back from the project to the root
* improve design, it has been done at the last minutes


# Improvements

There are few improvement we could do in the future

* we could use Hotwire for more responsiveness and collaboration
* rename a project
* allow comment formatting - markdown?
* delete a comment
* user authentication / authorisations 